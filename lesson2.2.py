import json
import re
import operator
from pprint import pprint

def get_data_from_json():
	with open ('newsafr.json', encoding = 'utf-8') as newsafr_file:
		news = json.load(newsafr_file)
		description_array = []

		for item in news['rss']['channel']['item']:
			description_array.append(item['description']['__cdata'])
	
	return (description_array)

def get_words_from_str(description, dictionary):
	words_array = re.findall(r'[а-яА-Я]{6,}', description)

	while len(words_array) > 0:
		word = words_array.pop()

		if word in words_array:
			if word in dictionary:
				dictionary[word] += 1
			else:
				dictionary[word] = 1
	return(dictionary)

def get_popular_words(count):
	dictionary = {}
	description_array = get_data_from_json()

	for description in description_array:
		dictionary = get_words_from_str(description, dictionary)

	print([x for x in reversed(sorted(dictionary, key=lambda key: dictionary[key]))][:count])
		
get_popular_words(10)